package main

import (
	"fmt"
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
	"log"
)

func rollback(tx *sql.Tx) {
	err := tx.Rollback()
	if err != nil {
		log.Println("Rollback")
	}
}

func main()  {
	db, err := sql.Open("mysql", "root:tongzhen@tcp(127.0.0.1:3306)/Data")
	if err != nil {
		panic(err.Error())
	}
	defer db.Close()

	tx,err := db.Begin()
	if err != nil {
		panic(err.Error())
	}
	defer rollback(tx)

	var sql string = "UPDATE user SET name = ? WHERE id = ?"
	updateInstance, err := db.Prepare(sql)
	if err != nil {
		panic(err.Error())
	}

	_, err1 := updateInstance.Exec("zhanghong1", 1)
	if err1 != nil {
		panic(err1.Error())
	}

	_, err2 := updateInstance.Exec("zhanghong2", 2)
	if err2 != nil {
		panic(err2.Error())
	}

	err3 := tx.Commit()
	if err3 != nil {
		panic(err3.Error())
	}

	fmt.Println("Transaction success")
}
