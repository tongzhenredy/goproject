package main

import (
	"fmt"
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
	//"math/rand"
)
// add data to database
func main() {
	db, err := sql.Open("mysql", "root:tongzhen@tcp(127.0.0.1:3306)/Data")
	if err != nil {
		panic(err.Error())
	}
	defer db.Close()

	var sql string = "INSERT INTO user(name, company) VALUES(?, ?)"
	insertInstance, err := db.Prepare(sql)
	if err != nil {
		panic(err.Error())
	}
	defer insertInstance.Close()

	//insert data
	_, err = insertInstance.Exec("tongzhen5", "sina")
	if err != nil {
		panic(err.Error())
	}

	fmt.Println("success insert.")
}
