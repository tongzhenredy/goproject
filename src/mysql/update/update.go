package main

import (
	"fmt"
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
)

func main()  {
	db, err := sql.Open("mysql", "root:tongzhen@tcp(127.0.0.1:3306)/Data")
	if err != nil {
		panic(err.Error())
	}
	defer db.Close()

	var sql string = "UPDATE user SET name = ? WHERE id = ?"
	updateInstance, err := db.Prepare(sql)
	if err != nil {
		panic(err.Error())
	}

	res, err := updateInstance.Exec("zhanghong", 5)
	if err != nil {
		panic(err.Error())
	}

	rowCount, err := res.RowsAffected()
	if err != nil {
		panic(err.Error())
	}
	fmt.Printf("affectId = %d\n", rowCount)
}
