-- phpMyAdmin SQL Dump
-- version 4.1.13
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: 2016-05-22 22:18:25
-- 服务器版本： 5.5.44-MariaDB-log
-- PHP Version: 5.5.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `dsp`
--
CREATE DATABASE IF NOT EXISTS `dsp` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `dsp`;

-- --------------------------------------------------------

--
-- 表的结构 `adexchanges`
--

DROP TABLE IF EXISTS `adexchanges`;
CREATE TABLE IF NOT EXISTS `adexchanges` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '名称',
  `dspid` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'dsp id',
  `deal_mode` tinyint(4) NOT NULL DEFAULT '0' COMMENT '交易模式：1RTB，2PMP，3PDB',
  `qps` int(11) NOT NULL DEFAULT '0' COMMENT 'qps',
  `qualification` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否需要资质：0不需要，1需要',
  `qualification_demand` varchar(600) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '资质要求',
  `audit` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否需要adx进行审核',
  `audit_mode` tinyint(4) NOT NULL DEFAULT '0' COMMENT '审核模式：0先审后投，1先投后审',
  `contact` varchar(30) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '联系人',
  `phone` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '电话',
  `valid` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否可用：0不可用，1可用',
  `token` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'adx分配密钥',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `adx_standard`
--

DROP TABLE IF EXISTS `adx_standard`;
CREATE TABLE IF NOT EXISTS `adx_standard` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `adx_id` int(11) NOT NULL DEFAULT '0' COMMENT 'adx id',
  `posid` int(11) NOT NULL DEFAULT '0' COMMENT '广告位id',
  `standard_id` int(11) NOT NULL DEFAULT '0' COMMENT '规范id',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `attachments`
--

DROP TABLE IF EXISTS `attachments`;
CREATE TABLE IF NOT EXISTS `attachments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '名称',
  `path` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '路径',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `cities`
--

DROP TABLE IF EXISTS `cities`;
CREATE TABLE IF NOT EXISTS `cities` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `adx_id` int(11) NOT NULL DEFAULT '0' COMMENT '交易平台id',
  `pid` int(11) NOT NULL DEFAULT '0' COMMENT '父级城市id',
  `name` varchar(30) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '名称',
  `code` varchar(20) COLLATE utf8_unicode_ci NOT NULL COMMENT '编码',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cities_adx_id_index` (`adx_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `clients`
--

DROP TABLE IF EXISTS `clients`;
CREATE TABLE IF NOT EXISTS `clients` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(90) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '客户名称',
  `homepage` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '客户主页',
  `vocation` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '行业编码',
  `category` tinyint(4) NOT NULL DEFAULT '0' COMMENT '客户类别：0广告主，1代理商',
  `zipcode` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '邮编',
  `desc` varchar(600) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '客户描述',
  `contact` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '联系人',
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '邮箱',
  `phone` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '电话',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `client_attachment`
--

DROP TABLE IF EXISTS `client_attachment`;
CREATE TABLE IF NOT EXISTS `client_attachment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL DEFAULT '0' COMMENT '客户id',
  `attachment_id` int(11) NOT NULL DEFAULT '0' COMMENT '附件id',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `client_attachment_client_id_index` (`client_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `client_audit`
--

DROP TABLE IF EXISTS `client_audit`;
CREATE TABLE IF NOT EXISTS `client_audit` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL DEFAULT '0' COMMENT '客户id',
  `adx_id` int(11) NOT NULL DEFAULT '0' COMMENT 'adx id',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '审核状态：0未通过，1通过',
  `reason` varchar(300) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '审核未通过原因',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `client_audit_client_id_index` (`client_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `creatives`
--

DROP TABLE IF EXISTS `creatives`;
CREATE TABLE IF NOT EXISTS `creatives` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(90) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '名称',
  `client_id` int(11) NOT NULL DEFAULT '0' COMMENT '客户id',
  `standard_id` int(11) NOT NULL DEFAULT '0' COMMENT '规范id',
  `content` text COLLATE utf8_unicode_ci NOT NULL COMMENT '创意内容',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `creative_attachment`
--

DROP TABLE IF EXISTS `creative_attachment`;
CREATE TABLE IF NOT EXISTS `creative_attachment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `creative_id` int(11) NOT NULL DEFAULT '0' COMMENT '创意id',
  `field` varchar(30) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '附件所属创意字段',
  `attachment_id` int(11) NOT NULL DEFAULT '0' COMMENT '附件id',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `creative_attachment_creative_id_index` (`creative_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `creative_audit`
--

DROP TABLE IF EXISTS `creative_audit`;
CREATE TABLE IF NOT EXISTS `creative_audit` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `creative_id` int(11) NOT NULL DEFAULT '0' COMMENT '创意id',
  `adx_id` int(11) NOT NULL DEFAULT '0' COMMENT 'adx id',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '审核状态：0未通过，1通过',
  `reason` varchar(300) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '未通过理由',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `creative_audit_creative_id_index` (`creative_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `dmps`
--

DROP TABLE IF EXISTS `dmps`;
CREATE TABLE IF NOT EXISTS `dmps` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `adx_id` int(11) NOT NULL DEFAULT '0' COMMENT 'adx id',
  `rule_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'rule id',
  `name` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '名称',
  `attachment_id` int(11) NOT NULL DEFAULT '0' COMMENT '附件id',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `groups`
--

DROP TABLE IF EXISTS `groups`;
CREATE TABLE IF NOT EXISTS `groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permissions` text COLLATE utf8_unicode_ci,
  `protected` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `groups_name_unique` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `interests`
--

DROP TABLE IF EXISTS `interests`;
CREATE TABLE IF NOT EXISTS `interests` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `adx_id` int(11) NOT NULL DEFAULT '0' COMMENT 'adx id',
  `pid` int(11) NOT NULL DEFAULT '0' COMMENT '父级分类',
  `name` varchar(30) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '名称',
  `code` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '兴趣编码',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `interests_adx_id_index` (`adx_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 转存表中的数据 `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2012_12_06_225988_migration_cartalyst_sentry_install_throttle', 1),
('2014_02_19_095545_create_users_table', 1),
('2014_02_19_095623_create_user_groups_table', 1),
('2014_02_19_095637_create_groups_table', 1),
('2014_02_19_160516_create_permission_table', 1),
('2014_02_26_165011_create_user_profile_table', 1),
('2014_05_06_122145_create_profile_field_types', 1),
('2014_05_06_122155_create_profile_field', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2016_03_09_072115_create_standards_table', 1),
('2016_03_09_072146_create_vocations_table', 1),
('2016_03_09_072233_create_interests_table', 1),
('2016_03_09_072311_create_adexchanges_table', 1),
('2016_03_09_072337_create_positions_table', 1),
('2016_03_09_072422_create_adx_standard_table', 1),
('2016_03_09_072444_create_clients_table', 1),
('2016_03_09_072524_create_attacmments_table', 1),
('2016_03_09_072558_create_client_attachment_table', 1),
('2016_03_09_072621_create_client_audit_table', 1),
('2016_03_09_072746_create_creatives_table', 1),
('2016_03_09_072814_create_creative_attachment_table', 1),
('2016_03_09_073026_create_creative_audit_table', 1),
('2016_03_09_073057_create_plans_table', 1),
('2016_03_09_073115_create_targets_table', 1),
('2016_03_09_073148_create_plan_datetime_table', 1),
('2016_03_09_073214_create_dmps_table', 1),
('2016_03_09_073242_create_plan_dmp_table', 1),
('2016_03_18_022629_create_cities_table', 1),
('2016_03_21_094832_create_standard_attachment_table', 1),
('2016_05_06_073101_add_columns_to_plans_table', 1),
('2016_05_17_103232_add_columns_to_plans_table', 2);

-- --------------------------------------------------------

--
-- 表的结构 `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- 表的结构 `permission`
--

DROP TABLE IF EXISTS `permission`;
CREATE TABLE IF NOT EXISTS `permission` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permission` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `protected` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `plans`
--

DROP TABLE IF EXISTS `plans`;
CREATE TABLE IF NOT EXISTS `plans` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `adx_id` int(11) NOT NULL DEFAULT '0' COMMENT '平台id',
  `client_id` int(11) NOT NULL DEFAULT '0' COMMENT '客户id',
  `creative_id` int(11) NOT NULL DEFAULT '0' COMMENT '创意id',
  `name` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '计划名称',
  `target_id` int(11) NOT NULL DEFAULT '0' COMMENT '定向id',
  `budget_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '预算类型：0总预算，1单日预算',
  `budget` int(11) NOT NULL DEFAULT '0' COMMENT '预算金额',
  `bid` int(11) NOT NULL DEFAULT '0' COMMENT '出价cpm，单位分',
  `exposure_monitor_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '第三方曝光监测地址',
  `click_monitor_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '第三方点击监测地址',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '状态：0暂停，1开启',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `plans_adx_id_index` (`adx_id`),
  KEY `plans_client_id_index` (`client_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `plan_datetime`
--

DROP TABLE IF EXISTS `plan_datetime`;
CREATE TABLE IF NOT EXISTS `plan_datetime` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `plan_id` int(11) NOT NULL DEFAULT '0' COMMENT '计划id',
  `type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '类型:0日期不限时间不限，1日期不限时间自定义，2日期自定义时间不限，3日期自定义时间自定义',
  `start_date` date NOT NULL DEFAULT '0000-00-00' COMMENT '开始日期',
  `end_date` date NOT NULL DEFAULT '0000-00-00' COMMENT '结束日期',
  `start_hour` tinyint(4) NOT NULL DEFAULT '0' COMMENT '开始小时',
  `end_hour` tinyint(4) NOT NULL DEFAULT '0' COMMENT '结束小时',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `plan_datetime_plan_id_index` (`plan_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `plan_dmp`
--

DROP TABLE IF EXISTS `plan_dmp`;
CREATE TABLE IF NOT EXISTS `plan_dmp` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `plan_id` int(11) NOT NULL DEFAULT '0' COMMENT '计划id',
  `dmp_id` int(11) NOT NULL DEFAULT '0' COMMENT 'dmp包id',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `plan_dmp_plan_id_index` (`plan_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `positions`
--

DROP TABLE IF EXISTS `positions`;
CREATE TABLE IF NOT EXISTS `positions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '名称',
  `adx_id` int(11) NOT NULL DEFAULT '0' COMMENT 'adx id',
  `price` int(11) NOT NULL DEFAULT '0' COMMENT '底价，单位分',
  `preferred_deal` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否优先交易：0否，1是',
  `charge_mode` tinyint(4) NOT NULL DEFAULT '0' COMMENT '计费模式：0cpm，1cpc',
  `access` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否接入：0否，1是',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `positions_adx_id_index` (`adx_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `profile_field`
--

DROP TABLE IF EXISTS `profile_field`;
CREATE TABLE IF NOT EXISTS `profile_field` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `profile_id` int(10) unsigned NOT NULL,
  `profile_field_type_id` int(10) unsigned NOT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `profile_field_profile_id_profile_field_type_id_unique` (`profile_id`,`profile_field_type_id`),
  KEY `profile_field_profile_field_type_id_foreign` (`profile_field_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `profile_field_type`
--

DROP TABLE IF EXISTS `profile_field_type`;
CREATE TABLE IF NOT EXISTS `profile_field_type` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `standards`
--

DROP TABLE IF EXISTS `standards`;
CREATE TABLE IF NOT EXISTS `standards` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '名称',
  `fields` text COLLATE utf8_unicode_ci NOT NULL COMMENT '字段',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `standard_attachments`
--

DROP TABLE IF EXISTS `standard_attachments`;
CREATE TABLE IF NOT EXISTS `standard_attachments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `standard_id` int(11) NOT NULL DEFAULT '0' COMMENT '规范id',
  `attachment_id` int(11) NOT NULL DEFAULT '0' COMMENT '附件id',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `standard_attachments_standard_id_index` (`standard_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `targets`
--

DROP TABLE IF EXISTS `targets`;
CREATE TABLE IF NOT EXISTS `targets` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '定向名称',
  `adx_id` int(11) NOT NULL DEFAULT '0' COMMENT 'adx id',
  `age` text COLLATE utf8_unicode_ci NOT NULL COMMENT '年龄',
  `gender` tinyint(4) NOT NULL DEFAULT '0' COMMENT '性别：0不限，1男，2女',
  `location` text COLLATE utf8_unicode_ci NOT NULL COMMENT '地域',
  `interest` text COLLATE utf8_unicode_ci NOT NULL COMMENT '兴趣',
  `network` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '网络',
  `platform` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '投放平台',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `throttle`
--

DROP TABLE IF EXISTS `throttle`;
CREATE TABLE IF NOT EXISTS `throttle` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned DEFAULT NULL,
  `ip_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `attempts` int(11) NOT NULL DEFAULT '0',
  `suspended` tinyint(1) NOT NULL DEFAULT '0',
  `banned` tinyint(1) NOT NULL DEFAULT '0',
  `last_attempt_at` timestamp NULL DEFAULT NULL,
  `suspended_at` timestamp NULL DEFAULT NULL,
  `banned_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `throttle_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permissions` text COLLATE utf8_unicode_ci,
  `activated` tinyint(1) NOT NULL DEFAULT '0',
  `banned` tinyint(1) NOT NULL DEFAULT '0',
  `activation_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `activated_at` timestamp NULL DEFAULT NULL,
  `last_login` timestamp NULL DEFAULT NULL,
  `persist_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `reset_password_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `protected` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `users_activation_code_index` (`activation_code`),
  KEY `users_reset_password_code_index` (`reset_password_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `users_groups`
--

DROP TABLE IF EXISTS `users_groups`;
CREATE TABLE IF NOT EXISTS `users_groups` (
  `user_id` int(10) unsigned NOT NULL,
  `group_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- 表的结构 `user_profile`
--

DROP TABLE IF EXISTS `user_profile`;
CREATE TABLE IF NOT EXISTS `user_profile` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `code` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `vat` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `first_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zip` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `avatar` blob,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_profile_user_id_foreign` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `vocations`
--

DROP TABLE IF EXISTS `vocations`;
CREATE TABLE IF NOT EXISTS `vocations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `adx_id` int(11) NOT NULL DEFAULT '0',
  `pid` int(11) NOT NULL DEFAULT '0' COMMENT '父级分类id',
  `code` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '行业编码',
  `name` varchar(30) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '名称',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `vocations_adx_id_index` (`adx_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

--
-- 限制导出的表
--

--
-- 限制表 `profile_field`
--
ALTER TABLE `profile_field`
  ADD CONSTRAINT `profile_field_profile_field_type_id_foreign` FOREIGN KEY (`profile_field_type_id`) REFERENCES `profile_field_type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `profile_field_profile_id_foreign` FOREIGN KEY (`profile_id`) REFERENCES `user_profile` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- 限制表 `user_profile`
--
ALTER TABLE `user_profile`
  ADD CONSTRAINT `user_profile_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
