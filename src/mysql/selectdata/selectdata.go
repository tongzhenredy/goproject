package main

import (
	"fmt"
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
)
// add data to database
func main() {
	db, err := sql.Open("mysql", "root:tongzhen@tcp(127.0.0.1:3306)/Data")
	if err != nil {
		panic(err.Error())
	}
	defer db.Close()
	//select

	var sql string = "SELECT name,company FROM user WHERE id = ?"
	selectInstance, err := db.Prepare(sql)
	if err != nil {
		panic(err.Error())
	}
	defer selectInstance.Close()

	var name,company string
	err = selectInstance.QueryRow(3).Scan(&name, &company)
	if err != nil {
		panic(err.Error())
	}
	fmt.Printf("name is: %s, company is: %s\n", name, company)
}