package main

import (
	"fmt"
	"github.com/garyburd/redigo/redis"
	"log"
)


func main() {
	conn, err := redis.Dial("tcp", "127.0.0.1:6379")
	if err != nil {
		log.Println("Reids dial error")
	}
	defer conn.Close()
	data, err := redis.String(conn.Do("GET", "name"))

	/*
	conn.Send("GET", "name")
	conn.Flush()
	data, err  := redis.String(conn.Receive())
	*/
	if err != nil {
		log.Println("Redis Get Error")
	}
	fmt.Println("data:", data)

}
