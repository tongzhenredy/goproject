package main

import (
	"fmt"
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
)

func main()  {
	db, err := sql.Open("mysql", "root:tongzhen@tcp(127.0.0.1:3306)/Data")
	if err != nil {
		panic(err.Error())
	}
	defer db.Close()

	//user prepare
	var sql string = "DELETE FROM user WHERE id = ?"
	deleteInstance, err := db.Prepare(sql)
	if err != nil {
		panic(err.Error())
	}

	_, errorMsg := deleteInstance.Exec(7)//delete id=7
	if errorMsg != nil {
		panic(err.Error())
	}

	fmt.Println("Success delete")
}
